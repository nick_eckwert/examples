#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <asm/unistd.h>
#include <sys/time.h>

#define N 100000000

volatile pid_t myid = 0;

// Simplest form of a C function
pid_t get_mypid() {
	return myid;
}

int main(int argc, char** argv)
{
	struct timeval start, end;

	// initialize myid
	myid = getpid();

	// measure overhead of a C function
	// (compiler optimization is disabled)
	gettimeofday(&start, NULL);
	for(unsigned int i = 0; i < N; i++) {
		myid = get_mypid();
	}
	gettimeofday(&end, NULL);

	printf("average time to determine pid (getpid): %lf usec\n", ((end.tv_sec - start.tv_sec) * 1000000 + end.tv_usec - start.tv_usec) / (double) N);


	// measure overhead of a system call
	gettimeofday(&start, NULL);
	for(unsigned int i = 0; i < N; i++) {
		asm volatile (
			"syscall"
			// output: return value in RAX, copy result to myid
			: "=a" (myid)
			// input: system call number described by RAX
			:  "0"(__NR_getpid)
			// clobber list: all registers, except rcx and r11, are preserved during the system call
			: "rcx", "r11", "memory"
		);
	}
	gettimeofday(&end, NULL);

	printf("average time to determine pid (syscall): %lf usec\n", ((end.tv_sec - start.tv_sec) * 1000000 + end.tv_usec - start.tv_usec) / (double) N);

	return 0;
}
