#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <string.h>

static char *socket_path = "./gin3-socket";

int main(int argc, char** argv) {
  struct sockaddr_un addr;
  char buf[100];
  int fd, cl, rc;

  // create a unix domain socket
  if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("socket error");
    exit(EXIT_FAILURE);
  }

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);

  // delete previous files
  unlink(socket_path);

  // bind socket to address
  if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    perror("bind error");
    exit(EXIT_FAILURE);
  }

  if (listen(fd, 5) == -1) {
    perror("listen error");
    exit(EXIT_FAILURE);
  }

  // wait for connection, client address isn't required => pass NULL to the OS
  if ( (cl = accept(fd, NULL, NULL)) == -1) {
    perror("accept error");
    exit(EXIT_FAILURE);  
  }

  // read message
  while ((rc=read(cl, buf, sizeof(buf))) > 0) {
    printf("read %u bytes: %.*s\n", rc, rc, buf);
  }
  if (rc == -1) {
    perror("read");
    exit(EXIT_FAILURE);
  } else if (rc == 0) {
    close(cl);
    close(fd);
  }

  return 0;
}
