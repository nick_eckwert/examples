#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include "ctest.h"

extern void call_foo(int arg);

int main(int argc, char **argv)
{
	call_foo(42);

	return 0;
}
