#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include "foo.h"

extern void call_foo(int arg);

int main(int argc, char **argv)
{
	foo(42);

	return 0;
}
