
#include <errno.h>
#include <linux/audit.h>
#include <linux/bpf.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <linux/unistd.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/prctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static int install_filter(int nr, int arch, int error) {
	struct sock_filter filter[] = {
		BPF_STMT(BPF_LD + BPF_W + BPF_ABS, (offsetof(struct seccomp_data, arch))),
		BPF_JUMP(BPF_JMP + BPF_JEQ + BPF_K, arch, 0, 3),
		BPF_STMT(BPF_LD + BPF_W + BPF_ABS, (offsetof(struct seccomp_data, nr))),
		BPF_JUMP(BPF_JMP + BPF_JEQ + BPF_K, nr, 0, 1),
		BPF_STMT(BPF_RET + BPF_K, SECCOMP_RET_ERRNO | (error & SECCOMP_RET_DATA)),
		BPF_STMT(BPF_RET + BPF_K, SECCOMP_RET_ALLOW),
	};
	struct sock_fprog prog = {
		.len = (unsigned short)(sizeof(filter) / sizeof(filter[0])),
		.filter = filter,
	};
	if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0)) {
		perror("prctl(NO_NEW_PRIVS)");
		return 1;
	}
	if (prctl(PR_SET_SECCOMP, 2, &prog)) {
		perror("prctl(PR_SET_SECCOMP)");
		return 1;
	}
	return 0;
}

int main(int argc, char *argv[]) {
	pid_t id;

	printf("Enable sandbox!\n");

	install_filter(__NR_clone, AUDIT_ARCH_X86_64, EPERM);
	install_filter(__NR_fork, AUDIT_ARCH_X86_64, EPERM);

	id = fork();
	if (id == -1) {
		perror("fork");
		exit(EXIT_FAILURE);
	}

	if (id == 0) {
		printf("Hello from child %d\n", getpid());
	} else {
		wait(NULL);                /* Wait for child */
		printf("Hello from parent %d\n", getpid());
	}

	exit(EXIT_SUCCESS);
}
