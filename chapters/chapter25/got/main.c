#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void dummy(char* str)
{
	system(str);
}

int main(int argc, char** argv)
{
	void* ptr = 0;
	char array[8];

	strcpy(array, argv[1]);
	memset(ptr, 0x00, sizeof(void*));
	strcpy(ptr, argv[2]);
	printf("Array at %p, ptr at %p, and ptr points to %p\n", array, &ptr, ptr);

	return 0;
}
