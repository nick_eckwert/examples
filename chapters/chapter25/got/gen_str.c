#include <stdio.h>
#include <unistd.h>

// Big Endian:         0000000000404028
char str1[] ="12345678\x28\x40\x40\x00";
// Big Endian:         0000000000401156
char str2[] =        "\x56\x11\x40\x00";

int main()
{
	FILE* fd;

	fd = fopen("./str1.txt", "w+");
	fprintf(fd, "%s", str1);
	fclose(fd);

	fd = fopen("./str2.txt", "w+");
	fprintf(fd, "%s", str2);
	fclose(fd);

	return 0;
}
