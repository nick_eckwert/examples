#include <stdlib.h>
#include <unistd.h>

static char message[] = "Hello students!\n";

int main(int argc, char** argv) {
    write(STDOUT_FILENO, message, 16);
    exit(EXIT_SUCCESS);
}
